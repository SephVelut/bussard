# Glossary

Members of the lab team won't use as much slang, but out when you're
talking with random folks on the newsgroups or job boards you'll hear
plenty of this.

* "panjandrums": government officials
* "Net of a million lies": the Internet
* "sixed": killed
* "crocla": mild expletive (feces)
* "nopping": idling
* "cons": construct
* "parse": understand
* "zero" (verb): wipe, erase
* "g-well": world
* "lock": portal

* "Hreðjar Óðins": expletive (odin's balls)
* "พระ อาเจียน ลิง": expletive (holy monkey vomit)
* "พัด ขี้ ควาย": expletive (stir-fried water buffalo turds)
* "ท้อง เสีย ช้าง": expletive (elephant diarrhea)
* "Ma gavte la nata": get over yourself (extremely rude)

Most of the vocabulary is widely-used, but some turns of phrases (the
non-english ones) might just be used by certain characters. Nari uses the Thai
ones, for instance.
