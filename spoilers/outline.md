# Plot outline

## Act I: Prologue

* Come to your senses in a remote mining system, email indicator blinking
* Lab team left cryptic message wishing you best (msgs/readme.md)
* After a short time, get Strauss's message about encrypted journals (msgs/strauss-1.msg)
* (optional) Buy a mining laser, do some mining to earn a bit of cash?
* Do some cargo runs or missions within Tana
 * You can't leave Tana because you can't get a security check needed for Sol portal clearance
* You can probably decrypt the rot13 journal on your own, but you can wait for act 2
* Try to accept a passenger mission, mission aborts due to not having life-support
* Buy life support, run a passenger mission or two (optional missions: passenger1, passenger3)
* Eventually accept mission to take Nari home (mission: passenger2)
* She figures out that whoa holy crap; you're a machine consciousness
* Nari helps you pass the security check needed for the portal
* You bring Nari to Newton Station, where she contacts an old friend who has clues

## Act II: Plugging into Subnet

Nari still doesn't have access to systems beyond Sol/Tana, but once you get
access to subnet, you can accept a mission which can get you access to
Terran/Bohk systems.

* Nari helps you decrypt the rot13 journal (mission: decrypt)
 * The one that records when Traxus first contacts you
* Whether you succeed or not with the rot13, she eventually forwards you a
  message about connecting to subnet where you can find more (message: subnet.msg)
* After a short delay, you get another message with a bit more details about subnet.
* Once you get on subnet, you find a mission to find Dorath.
 * but you can't accept it directly from subnet.
 * also on subnet: code to sync to your mail client.
  * once the message is in your client, you can accept the mission.

## Act III: Spacetime Junction

Characters (especially Dorath) should be deeply suspicious of Traxus,
assuming that he is a villain opposed to humanity. This isn't true at
all, but it fits the facts as they know them.

Need to build up the spacetime junction as if it's the endgame
goal. "Unimaginable power; the ability to explore the spacetime
continuum!" So when it turns out Traxus found something else much more
important to pursue, (right before you actually get it), it should be
a bit of a twist.

* Clay leads you on a trail that eventually gets you to finding the spacetime junction
* Then he betrays you to get revenge on his former lab mates--boo!
 * Tries to pin the theft on them?
* A ship comes in to intercept you; if they reach you and board your ship it's game-over
* But you can use the spacetime junction to reset back to the point where you first find it
 * Maybe Nari installs the junction so it triggers automatically if you're in critical danger
 * Somehow you use this trick to avert capture?
* Even so you're blacklisted from using portals to the Terran systems

TODO: need a ton more detail here

## Act IV: Gathering clues

Either here or in the previous act we need to find out that Traxus is
not actually evil. This can be revealed as Mila reveals some of her
stories from working with Ikon technologies, and as you understand
that Clay may not have been the most reliable source.

At this point, progress up to the endgame section should consist only
of learning new things, because now that you can reset the game state
with the junction (and will need to reset the game state to solve some
puzzles) changes to the state of things outside your ship will be
thrown away.

Threads to explore:

* Tracking down logs from the MC research team that created you
* Learning more about the portal system and its Lisp OS
* Find out that Ikon is a shell company for Traxus
 * You should have heard about Ikon a fair bit before finding this out

Can we find a way to make auto-pilot functionality necessary to the plot?
Purchase additional ships and load them up with auto-pilot?

## Act V: Ikon Technologies and the Domain Injector

* Find out that Traxus has colonized his own world
* Learn how to log into portal computer console
 * Subvert security features; allow portal to secret colony

## Loose threads so far

Possibly worth pursuing, but not worked into the overall storyline yet.

* Sol MC
* Traxus crash
* Rampancy
* Dr. May's logs
* Dr. Sacar's logs
